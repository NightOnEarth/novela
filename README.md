
# Usage of events in Laravel

The code presented here demonstrates the use of events in the Laravel framework. 

### Implemented real life example
After a record has been inserted into the database a notification email should be sent to 
selected users. 
This code has been implemented on the site: 
https://taxisharers.com/stone/novela/1
The event will be triggered off when the user sends a comment though a feedback form.

#### Please note
This repository does not include the complete application, but only the sources relevant 
to the event notification: 

## Event
app/Events/FeedbackCreated.php

The event should be dispatched each time a feedback has been created. 
Feedback is our database model with the table containing user remarks entered 
via web form/ajax. The constructor receives the Feedback as a parameter.

## Listener
app/Listeners/FeedbackCreatedListener.php

The listener... guess what... listens to the events. In order to be able to listen it must receive the event as a parameter in the handle() function.
This function will fetch some data (like email addresses of the recipient(s)) and will despatch the Notification.

## Providers
app/Providers/EventServiceProvider.php

We must register listeners by adding one more elements to the **$listen** array in the EventServiceProvider

    \App\Events\FeedbackCreated::class => [
        \App\Listeners\FeedbackCreatedListener::class,

In this case we only need one listener class: for the case when a feedback has been created. But events can have 
multiple listeners, for example if a record has been modified or deleted.


## Controller
app/Http/Controllers/NovelaController.php

Where to fire the event? One way would be to include it inside the Feedback model class, 
inside a $dispatchesEvens array. Here, however, we have chosen a more transparent way, 
by dispatching the event inside the controller's insertFeedback() function:

    event( new \App\Events\FeedbackCreated( $feedback));

## Notifications
app/Notifications/FeedbackCreatedNotification.php

The notification class must receive the model (in our case Feedback) as a parameter of 
its constructor. Here, in the **toMail()** function you can also modify the content of the email itself.
---

#### Credit
Inspired by excellent video of Povilas Korop:
https://www.youtube.com/watch?v=DvoaU6cQQHM

