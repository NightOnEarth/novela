<!doctype html>
<html lang="zh-Hans">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>尸体在旅馆: 中国学生写的短篇小说</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ma+Shan+Zheng&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
{{--    for rating stars--}}
    <link href="{{ asset('css/rating.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 55vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-family: 'Ma Shan Zheng', cursive;
            font-size: 70px;
            color: #ff0000;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
        .ma {
            font-family: 'Ma Shan Zheng', cursive;
            font-size: 50px;
            /*color: #ff4500;*/
        }
        .foreign {
            font-family: 'Nunito', sans-serif;
            font-size: 23px;
            /*color: #ff4500;*/
        }
        .dialog {
            font-family: 'Nunito', sans-serif !important;
            font-size: 15px !important;
        }
        .emlink {
            font-weight: bold !important;
            color: #ff0000 !important;
        }
        .userFeedback {
            padding-top:3px;padding-bottom:3px;clear: both;border-top: 1px dotted #fff;
        }
        .feedbackTxt {
            float: left;margin-bottom:0;width: 90%;overflow-wrap: break-word;font-size: smaller
        }
        .feedbackStars {
            float: right;margin-bottom:0;font-size: smaller
        }
    </style>
    <style type="text/css">
        .tooltip-c {
            border-bottom: 1px dotted #000000; color: #000000; outline: none;
            cursor: help; text-decoration: none;
            position: relative;
        }
        .tooltip-c span {
            margin-left: -999em;
            position: absolute;
        }
        .tooltip-c:hover span {
            border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            position: absolute; left: 1em; top: 2em; z-index: 99;
            margin-left: 150px; width: 450px;
        }
        .tooltip-c:hover img {
            /*border: 0; margin: -10px 0 0 -55px;*/
            border: 0; margin: -20px -20px 0 -55px;
            float: left; position: absolute;
        }
        .tooltip-c:hover em {
            font-family: Candara, Tahoma, Geneva, sans-serif; font-size: 1.2em; font-weight: bold;
            display: block; padding: 0.2em 0 0.6em 0;
        }
        .tooltip-d {
            border-bottom: 0; color: #000000; outline: none;
            cursor: help; text-decoration: none;
            position: relative;
        }
        .tooltip-d span {
            margin-right: 1em;
            position: fixed;
            top: 150px;
            left: 40%;
            display: none;
        }
        .tooltip-d:hover span {
            border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            display: block;
            z-index: 99;
            width: 450px;
        }
        .tooltip-d:hover img {
            /*border: 0; margin: -10px 0 0 -55px;*/
            border: 0; margin: -20px -20px 0 -55px;
            float: left; position: absolute;
        }
        .tooltip-d:hover em {
            font-family: Candara, Tahoma, Geneva, sans-serif; font-size: 1.2em; font-weight: bold;
            display: block; padding: 0.2em 0 0.6em 0;
        }


        .classic { padding: 0.8em 1em; }
        .custom { padding: 0.5em 0.8em 0.8em 2em; }
        * html a:hover { background: transparent; }
        .classic {background: #FFFFAA; border: 1px solid #FFAD33; }
        .critical { background: #FFCCAA; border: 1px solid #FF3334;	}
        .help { background: #9FDAEE; border: 1px solid #2BB0D7;	}
        .info { background: #9FDAEE; border: 1px solid #2BB0D7;	}
        .warning { background: #FFFFAA; border: 1px solid #FFAD33; }
    </style>

</head>
<body>
<div class="flex-center position-ref">
    <div class="content">
        <div class="m-b-md">
            <h1 class="title text-center">尸体在旅馆</h1>
            <p>&copy; Richard Urban</p>
        </div>

        @if( $chapter == 0)
            <main class="py-4">
                @yield('content')
            </main>
        @endif

        <div class="links">
            <a href="./1" class="{{$chapter == 1 ? 'emlink' : ''}}">Chapter 1</a>
            <a href="./2" class="{{$chapter == 2 ? 'emlink' : ''}}">Chapter 2</a>
            <a href="./3" class="{{$chapter == 3 ? 'emlink' : ''}}">Chapter 3</a>
            <a href="./4" class="{{$chapter == 4 ? 'emlink' : ''}}">Chapter 4</a>
            <a href="./5" class="{{$chapter == 5 ? 'emlink' : ''}}">Chapter 5</a>
        </div>

{{--   may be set in route--}}
        @if ( session('please') )
            <div class="alert alert-warning pull-left" id="earlierPlease">
                <h3 lang="en"> Please read earlier chapter ({{ session('please') }}) first</h3>
            </div>
        @endif
    </div>
</div>

<!-- Custom Modal for images -->
<div class="modal fade" id="sshot" tabindex="-1" data-keyboard="false" data-backdrop="false"
     role="dialog" aria-labelledby="sshot" aria-hidden="true">

    <div class="modal-dialog modal-lg col-lg-12" role="document">
        <div class="modal-content">

            <div class="modal-header">
                Handwritten:
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #ff0000;">&times;</span>
                </button>
            </div>

            <div class="modal-body text-center" style="background-color: #c0c0c0;">
                <img src="" class="img-fluid rounded" id="modBodyPic">
            </div>
        </div>
    </div>
</div>
<!-- Custom Modal Form ends -->


@if( $chapter > 0)
    <main class="py-4">
        @yield('content')
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/jquery/jquery.min.js"><\/script>')</script>


    <script src="https://www.holacuba.de/classes/assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js"></script>
    <script src="https://www.holacuba.de/classes/assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
    <script src="https://www.holacuba.de/classes/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>

    <script src="https://taxisharers.com/assets/bootstrap413/js/bootstrap.bundle.min.js"></script>
    <script type="application/javascript">
    jQuery(document).ready(function() {

        if ($('#earlierPlease').length) {
            $('#earlierPlease').show().delay(6000).fadeOut()
        }

        $( "#text_zh,#en_button" ).click(
            function( event) {
                event.preventDefault();
                $("#leftSide").removeClass('col-lg-8').addClass('col-lg-4');
                $("#rightSide").removeClass('col-lg-4').addClass('col-lg-8');
                $("#text_en").fadeIn(1100).show();
                $("#en_button, #feedback-form").hide();
            }
        );

        $( "#close_button" ).click(
            function( event) {
                event.preventDefault();
                $("#rightSide").removeClass('col-lg-8').addClass('col-lg-4');
                $("#leftSide").removeClass('col-lg-4').addClass('col-lg-8');
                $("#text_en").hide();
                $("#en_button, #feedback-form").show();
            }
        );


        $( "#handwriting" ).click(
            function( event) {
                event.preventDefault();

                var chapter_number = $( "#chapter_number" ).val();
                if ( chapter_number === '1') {
                    var picture = 'https://taxisharers.com/stone/public/img/chapter-' + chapter_number + '.jpg';
                    $('#modBodyPic').attr( 'src', picture);
                    $('#sshot').modal();
                } else {
                    alert('I am still working on the handwritten pages.\n我仍在手写页面上');
                }
            }
        );


        $( "#form-button" ).click( function (event) {

            //gather data
            var whatnext    = $( "#whatnext" ).val().trim();
            var rating      = $('input[name="reliability"]:checked').val();
            rating          = isNumeric( rating) ? rating : 0;
            if( whatnext === '' && rating < 1) {
                $("#formError").text('Please give us some feedback. Thank you!').show().delay(5000).fadeOut();
                return false;
            }

            var data = JSON.stringify( {
                chapter_number : $( "#chapter_number" ).val(),
                whatnext : whatnext,
                rating : rating
            });

            // Content-Type header of the request is properly set to application/json

            var request = $.ajax({
                url: "process_feedback",
                method: "POST",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                    'Content-Type' : 'application/json'
                },
                data: data,
                cache : false
            });

            request.done(function( msg ) {
                if( msg.substr(0,5) === 'ERROR') {
                    $("#formError").text(msg).show().delay(5000).fadeOut();

                } else {
                    $("#form-button").hide();
                    $("#formSuccess").html('<h5>Thank you. 谢谢</h5>').show().delay(5000).fadeOut();

                    var jsonObject = JSON.parse(msg);
                    var userFeedback = '';
                    $.each(jsonObject, function(key, value){
                        userFeedback += feedbackHtml( value.comment, value.rating);
                    });
                    userFeedback += '<br>&nbsp;';
                    $("#userFeedback").html( '<b>Reader\'s comments:</b>' + userFeedback).show();
                }
            });

            request.fail(function( jqXHR, textStatus ) {

                $("#formError").text('Sorry, error encountered. Please try again').show().delay(5000).fadeOut();
            });
        });

    });

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function feedbackHtml( comment, rating) {
        if ( comment.trim() === '') {
            return '';
        }
        var rateStars = '';
        if ( isNumeric(rating) && rating > 0) {
            rateStars = '<p class="feedbackStars"> ' + rating + ' <i class="fa fa-star"></i></p>';
        }
        return '' +
            '<div class="userFeedback">' +
            rateStars +
            '<p class="feedbackTxt"><i class="fa fa-user" style="color: #ffffff;"></i> ' + comment + '</p>' +
            '</div>';
    }
    </script>
@endif
</body>
</html>
