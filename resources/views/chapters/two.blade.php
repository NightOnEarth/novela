@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-8" id="leftSide">
                <h2 class="ma" style="background-color: #c1c1c1;float: right">二</h2>
                <h2 lang="en">Chapter Two</h2>
                <p class="ma" id="text_zh">半小时后敏，女服务员，再次进入房间。
                    <br>男死了。
                    <br>她知道钱在塑料袋里。她接过。
                    <br>敏迅速离开房间，用毛巾盖住袋子。
                </p>
                @include( 'chapters.subviews.goto_buttons')
            </div>
            <div class="col-lg-4" id="rightSide">

                @include('chapters.subviews.flag_form')

                <div id="text_en" class="custom info" style="display: none">

                    @include('chapters.subviews.flag_close_buttons')

                    <p class="foreign" lang="en">
                       Half an hour later Min, the waitress, entered the room again.
                       <br>The man was dead.</p>
                    <p class="foreign" lang="en">
                        She knew the money was in the plastic bag. She took it.</p>
                    <p class="foreign" lang="en">
                        Min has left the room swiftly, using hand towel to cover the bag.</p>
                    <hr>
                    @include( 'chapters.subviews.goto_buttons')
                </div>
            </div>
        </div>
    </div>
@endsection
