@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-8" id="leftSide">
                <h2 class="ma" style="background-color: #c1c1c1;float: right">三</h2>
                <h2 lang="en">Chapter Three</h2>
                <p class="ma" id="text_zh">敏走下楼梯。
                    <br>她注意到接待处的武装警察部队。警察在问死者。
                    <br>敏大喊：救命！ 18号房间的人病得很重。
                </p>

                @include( 'chapters.subviews.goto_buttons')
            </div>
            <div class="col-lg-4" id="rightSide">

                @include('chapters.subviews.flag_form')

                <div id="text_en" class="custom info" style="display: none">

                    @include('chapters.subviews.flag_close_buttons')

                    <p class="foreign" lang="en">
                        Min walked down the stairs.</p>
                    <p class="foreign" lang="en">
                        She noticed armed police unit at the reception desk.
                        The cops were asking for the dead man.</p>
                    <p class="foreign" lang="en">
                        Min yelled: Help! The man in room 18 is very sick.</p>
                    <hr class="clearfix" lang="en">
                    @include( 'chapters.subviews.goto_buttons')

                </div>
            </div>
        </div>
    </div>
@endsection
