@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-8" id="leftSide">
                <h2 class="ma" style="background-color: #c1c1c1;float: right">一</h2>
                <h2 lang="en">Chapter One</h2>

                <p class="ma">旅馆客人要求在房间喝咖啡。</p>
                <p class="ma">女服务员毒化了咖啡。</p>

                @include( 'chapters.subviews.goto_buttons')
            </div>
            <div class="col-lg-4" id="rightSide">

                @include('chapters.subviews.flag_form')

                <div id="text_en" class="custom info" style="display: none">

                    @include('chapters.subviews.flag_close_buttons')

                    <p class="foreign" lang="en">
                        Hotel guest asked for coffee in his room.</p>
                    <p class="foreign" lang="en">
                        The waitress poisoned the coffee.</p>
                    <hr class="clearfix" lang="en">
                    @include( 'chapters.subviews.goto_buttons')

                </div>
            </div>

        </div>
    </div>
@endsection
