@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-8" id="leftSide">
                <h2 class="ma" style="background-color: #c1c1c1;float: right">四</h2>
                <h2 lang="en">Chapter Four</h2>
                <p class="ma" id="text_zh">
                    敏已经被讯问，但没有被逮捕。
                    死者是他的帮派出卖的黑帮。
                    他的死被归类为自杀。
                </p>

                @include( 'chapters.subviews.goto_buttons')
            </div>
            <div class="col-lg-4" id="rightSide">

                @include('chapters.subviews.flag_form')

                <div id="text_en" class="custom info" style="display: none">

                    @include('chapters.subviews.flag_close_buttons')

                    <p class="foreign" lang="en">
                        Min has been interrogated, but not arrested.
                        The dead man was a gangster betrayed by his gang.
                        His death has been classified as suicide.
                    </p>
                    <hr>
                    @include( 'chapters.subviews.goto_buttons')

                </div>
            </div>
        </div>
    </div>
@endsection
