@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-5" id="leftSide">
                <p class="text-left" lang="en">This very short story has been written by a student of Chinese
                    (and the Laravel framework enthusiast).</p>
                <p class="text-left" lang="en">Please enjoy and comment!</p>
            </div>
            <div class="col-lg-5" id="rightSide">
                <p class="text-left">这个非常短的故事是由一个中国语言学生 (和 Laravel 框架爱好者）写的。</p>
                <p class="text-left">请欣赏并发表评论。</p>
            </div>
        </div>
    </div>
@endsection
