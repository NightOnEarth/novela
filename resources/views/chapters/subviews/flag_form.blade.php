
                <button type="button" class="btn btn-lg" id="en_button"
                        style="float: right;background-repeat: no-repeat;background-image: url({{asset('public/img/uk40x26.jpg')}});">&nbsp;</button>
                <button type="button" class="btn btn-lg" id="handwriting"
                        style="float: right;background-repeat: no-repeat;background-image: url({{asset('public/img/character40x41.jpg')}});">&nbsp;</button>

{{--                <button type="button" class="btn btn-lg" id="handwriting_button"--}}
{{--                        style="margin-top:-3px;float: right;background-repeat: no-repeat;background-image: url(https://taxisharers.com/stone/public/img/hp40x40.jpg);">&nbsp;</button>--}}
                <br style="clear: both">
                <form class="sky-form" id="feedback-form"
                      style="background-color: #d3d3d3; margin-top:2.5rem;padding: 8px;">
                    @csrf
                    @method('POST')
                    <label for="whatnext" class="dialog">What do you think will happen next?
                            <br>您认为接下来会发生什么？</label>
                    <input type="text" name="whatnext" id="whatnext" style="width: 100%"
                        value="{{ $request->whatnext }}">
                    <br style="clear: both"><br>
                    <div class="rating" style="text-align: left;">
                        <p class="dialog" style="float: left;">Rate the story:</p>
                        <input type="radio" name="reliability" id="reliability-5"
                           {{ $request->reliability == 5 ? 'CHECKED' : '' }} value="5">
                        <label for="reliability-5"><i class="fa fa-star"></i></label>

                        <input type="radio" name="reliability" id="reliability-4"
                           {{ $request->reliability == 4 ? 'CHECKED' : '' }} value="4">
                        <label for="reliability-4"><i class="fa fa-star"></i></label>

                        <input type="radio" name="reliability" id="reliability-3"
                           {{ $request->reliability == 3 ? 'CHECKED' : '' }} value="3">
                        <label for="reliability-3"><i class="fa fa-star"></i></label>

                        <input type="radio" name="reliability" id="reliability-2"
                           {{ $request->reliability == 2 ? 'CHECKED' : '' }} value="2">
                        <label for="reliability-2"><i class="fa fa-star"></i></label>

                        <input type="radio" name="reliability" id="reliability-1"
                           {{ $request->reliability == 1 ? 'CHECKED' : '' }}  value="1">
                        <label for="reliability-1"><i class="fa fa-star"></i></label>
                    </div>
                    <div style="clear: both;text-align: center">
                        <input type="hidden" id="chapter_number" value="{{$chapter}}">
                        <button type="button" class="btn btn-sm btn-dark text-center"
                            id="form-button">Send</button>
                    </div>
                    <div class="alert alert-danger small" id="formError" style="display: none"></div>
                    <div class="alert alert-success small" id="formSuccess" style="display: none"></div>
                    <div id="userFeedback" style="display: none"></div>
                </form>
