
                @if ($chapter < 5)

                <div class="row justify-content-center">
                    <div class="col-lm-6">
                        <a href="./{{ $chapter+1 }}">
                            <button type="button" class="btn btn-sm btn-warning pull-left"
                                    style="margin-right:.2rem;margin-left:.2rem;width: 7rem;">Go to chapter {{ $chapter+1 }}</button>
                        </a>
                    </div>
                    <div class="col-lm-6">
                        <a href="./{{ $chapter+1 }}">
                            <button type="button" class="btn btn-sm btn-warning pull-right"
                                    style="margin-right:.2rem;margin-left:.2rem;width: 7rem;">第 {{ $chapter+1 }} 章</button>
                        </a>
                    </div>
                </div>

                @endif
