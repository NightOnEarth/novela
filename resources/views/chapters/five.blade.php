@extends('layouts.book')

@section('content')
    <div class="container" id="txtop">
        <div class="row justify-content-center">
            <div class="col-lg-8" id="leftSide">
                <h2 class="ma" style="background-color: #c1c1c1;float: right">五</h2>
                <h2 lang="en">Chapter Five</h2>
                <p class="ma" id="text_zh">
                    四天后，敏的公寓的门铃响了，警察调查员站在门口。
                    他说：你是杀手。
                    我注意到你已经把袋子里的钱盖了。
                    你愿意嫁给我吗？
                </p>
                <p class="ma" id="text_zh">
                    他们有两个孩子，幸福地生活在一起。
                </p>

                @include( 'chapters.subviews.goto_buttons')
            </div>
            <div class="col-lg-4" id="rightSide">

                @include('chapters.subviews.flag_form')

                <div id="text_en" class="custom info" style="display: none">

                    @include('chapters.subviews.flag_close_buttons')

                    <p class="foreign" lang="en">
                        Four days later a bell rang in Min's apartment.
                        The police investigator stand at the door.
                        He said: You are the killer.
                        I have noticed the money you have covered in the bag.
                        Will you marry me?
                    </p>
                    <p class="foreign" lang="en">
                        They had two children and lived happily together.
                    </p>
                    <hr>
                    @include( 'chapters.subviews.goto_buttons')

                </div>
            </div>
        </div>
    </div>
@endsection
