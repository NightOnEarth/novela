<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;

// DO NOT use Illuminate\Notifications\Notification.
// this was a game changer - using facade rathern then class as suggested in
// https://stackoverflow.com/questions/43706229/call-to-undefined-method-illuminate-notifications-notificationsend
use Notification;

use App\User;

use Illuminate\Queue\InteractsWithQueue;

use App\Notifications\FeedbackCreatedNotification;


class FeedbackCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle( \App\Events\FeedbackCreated $event)
    {

        $this->admins = User::select('id', 'name', 'email', 'role')
            ->where('role', 2)
            ->get();

        Notification::send( $this->admins, new FeedbackCreatedNotification( $event->feedback));
    }
}
