<?php

namespace App\Notifications;

use App\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FeedbackCreatedNotification extends Notification
{
    use Queueable;

    public $feedback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( Feedback $feedback)
    {
        //
        $this->feedback = $feedback;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->greeting('尸体在旅馆 getting attention!')
                    ->line('A new remark has been entered in the chinese book:')
                    ->line($this->feedback->comment)
                    ->line('Rating: '. $this->feedback->rating .
                        ' Chapter: '. $this->feedback->chapter)
                    ->action('Go to 尸体在旅馆', url('/novela'))
                    ->line('Proudly made with event notification!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
