<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// inserted for the notifications trait
use Illuminate\Notifications\Notifiable;

class Feedback extends Model
{
    //
    use Notifiable;

    protected $table = 'feedbacks';
}
