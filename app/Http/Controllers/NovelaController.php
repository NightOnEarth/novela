<?php

namespace App\Http\Controllers;

use App\Events\FeedbackCreated;
use Illuminate\Http\Request;
use App\Feedback;

class NovelaController extends Controller
{

    public function feedback(Request $request)
    {
        $errorMessage   = '';
        $userFeedbacks  = null;

        // checking user input obtained from ajax
        if( $this->verifyRequest( $request)) {
            // all clear
        } else {
            return 'ERROR: Insufficient Input';
        }

        // inserting feedback into database
        if( $this->insertFeedback( $request)) {
            // all clear

            // Notification should fire up

        } else {
            return 'ERROR: Sorry, our database is overcharged, please try again.';
        }

        // obtaining feedbacks from database (last 30 users)
        $feedback = $this->obtainFeedbacks( $request->input('chapter_number'), 30);
        if( $this->isJSON( $feedback)) {
            return $feedback;
        } else {
            // Error, because at least one feedback (from the current user) should be available
            return 'ERROR: Sorry, can\'t retrieve feedbacks, please try again.';
        }

    }


    // user should provide either text input or star rating (0 - 5) or both
    function verifyRequest( $request)
    {
        return
            $request->input('chapter_number') > 0
            &&
            (
                ! empty( trim($request->input('whatnext')))
                or
                ( ! empty( $request->input('rating'))
                    && is_numeric( $request->input('rating'))
                )
            );
    }

    /**
     *
     * Inserting user feedback to database
     */
    function insertFeedback( $request)
    {
        $feedback = new Feedback();

        $feedback->comment  = empty($request->whatnext) ? '' : $request->whatnext;
        $feedback->rating   = (int) $request->rating;
        $feedback->chapter  = $request->chapter_number;
        $feedback->ip2long  = ip2long( $_SERVER['REMOTE_ADDR']);

        event( new \App\Events\FeedbackCreated( $feedback));

        return $feedback->save();
    }

    /**
     * Retrieve User feedbacks for a specific chapter from the database
     */
    function obtainFeedbacks( $chapter, $maxFeedbacks=30)
    {

        $feedback = Feedback::select('created_at', 'comment', 'rating')
            ->where('chapter', (int) $chapter)
            ->where('chapter', '=', $chapter)
            ->orderBy('id', 'desc')
            ->limit($maxFeedbacks)
            ->get();

        return $feedback->toJson();
    }


    /**
     * @param $string
     * @return bool
     */
    function isJSON($string)
    {

        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }


}   // class NovelaController ends here
